import json
import argparse
import os
import glob
import subprocess
from datalake.datalake import Datalake

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('index', help='Upload to either the segment or video index', type=str)
parser.add_argument('dir', help='Directory with parsed scenes and the \'meta.json\' file', type=str)
args = vars(parser.parse_args())

asset_dir = os.path.abspath(args['dir'])
index = args['index']
if index not in ['video', 'segment']:
    raise ValueError('Invalid option: specify either `video` or `segment` as first argument')

dl = Datalake()

scene_metadata = {}
with open(os.path.join(asset_dir, 'meta.json')) as json_file:
    scene_metadata = json.load(json_file)

if not all(k in scene_metadata
           for k in ('vid_id', 'title', 'safetitle', 'desc', 'keywords', 'fps', 'width', 'height', 'slugs', 'url')):
    raise ValueError('\'meta.json\' does not contain the required keys.')

if index == 'segment':
    scenes = glob.glob(os.path.join(asset_dir, 'shot*/'))
    if len(scenes) < 1:
        raise ValueError('No scenes detected in the provided folder')

    scenes = [x for x in scenes if len(glob.glob(os.path.join(x, '*.jpg'))) > 0]

    for n, scene in enumerate(sorted(scenes)):
        total_frames = len(glob.glob(os.path.join(scene, '*.jpg')))
        asset_name = scene_metadata['safetitle'].lower().replace(' ', '_') + '_' + os.path.basename(os.path.dirname(scene))
        if scene_metadata['url'] != 'none':
            urlwtime = scene_metadata['url'] + '&t={}m{}s'.format( os.path.basename(os.path.dirname(scene)).split('_')[-2], os.path.basename(os.path.dirname(scene)).split('_')[-1])
        else:
            urlwtime = 'none'
        print('Creating asset with name: {}, frames: {}, fps: {}, url: {}'.format(asset_name, total_frames, scene_metadata['fps'], urlwtime))
        asset_id = dl.create_asset(asset_name=asset_name,
                                fps=scene_metadata['fps'],
                                frames_amount=total_frames,
                                slugs=scene_metadata['slugs'],
                                user_data={
                                    'original_url': urlwtime,
                                    'original_desc': scene_metadata['desc'],
                                    'original_tags': scene_metadata['keywords'],
                                    'camera_model': 'unknown'
                                })
        artifact_id = dl.add_artifact(asset_id=asset_id,
                                    artifact_name='rgb_orig_jpg',
                                    folder_path=scene,
                                    data_type=Datalake.DataType.ImagesTarRGB,
                                    metadata={
                                        'width': scene_metadata['width'],
                                        'height': scene_metadata['height'],
                                        'codec': 'JPEG'
                                    })
elif index == 'video':
    types = ('*.mp4', '*.webm')
    files_grabbed = []
    for files in types:
        files_grabbed.extend(glob.glob(os.path.join(asset_dir, files)))
    vidpath = list(files_grabbed)[0]
    print(list(files_grabbed))
    if 'num_frames' in scene_metadata: num_frames = scene_metadata['num_frames']
    else:
        print('No total framenumber in metadata, counting frames manually...')
        get_video_num_frames_cmdl = 'ffprobe -threads 8 -v error -count_frames -select_streams v:0 -show_entries stream=nb_read_frames -of default=nokey=1:noprint_wrappers=1 {}'.format(
            vidpath)
        process = subprocess.Popen(get_video_num_frames_cmdl, shell=True, stdout=subprocess.PIPE)
        process.wait()
        num_frames = int(process.stdout.readline().decode('utf-8').strip())
    assert(num_frames > 0)
    video_id = dl.create_video(video_name=scene_metadata['safetitle'],
                               fps=scene_metadata['fps'],
                               slugs=scene_metadata['slugs'],
                               frames_amount=num_frames,
                               user_data={
                                   'original_url': scene_metadata['url'],
                                   'original_desc': scene_metadata['desc'],
                                   'original_tags': scene_metadata['keywords'],
                                   'camera_model': 'unknown'
                               })
    desc_id = dl.add_video_descriptor(video_id=video_id,
                                      descriptor_name='Source file',
                                      video_type=Datalake.VideoType.VideoRGB,
                                      file_path=vidpath,
                                      metadata={
                                          'width': scene_metadata['width'],
                                          'height': scene_metadata['height'],
                                          'codec': scene_metadata['codec'],
                                          'codec_name': scene_metadata['codec_name'],
                                      })
