import argparse
import unicodedata
import string
import sys
import os
import subprocess
import re
import json
import time
import glob
import youtube_dl
import inquirer
import shutil
from fractions import Fraction
from decimal import Decimal
from pprint import pprint
from urllib.parse import urlparse
from pathlib import Path
from fractions import Fraction
from progress.bar import IncrementalBar
from progress.spinner import Spinner

bestformat = (  '(bestvideo[height=2160][fps>30]'
                '/bestvideo[height=2160]'
                '/bestvideo[height=1440][fps>30]'
                '/bestvideo[height=1440]'
                '/bestvideo[height=1080][vcodec^=avc1][fps>30]'
                '/bestvideo[height=1080][vcodec^=avc1]'
                '/bestvideo[vcodec^=avc1][fps>30]'
                '/bestvideo[vcodec^=avc1])'
                '/bestvideo')
# bestformat = (  '(bestvideo[height>2160][vcodec=vp9.2]'
#                 '/bestvideo[height>2160][vcodec=vp9]'
#                 '/bestvideo[height=2160][vcodec=vp9.2]'
#                 '/bestvideo[height=2160][vcodec=vp9][fps>30]'
#                 '/bestvideo[height=2160][vcodec=vp9]'
#                 '/bestvideo[height=1440][vcodec=vp9.2]'
#                 '/bestvideo[height=1440][vcodec=vp9][fps>30]'
#                 '/bestvideo[height=1440][vcodec=vp9]'
#                 '/bestvideo[height=1080][vcodec=vp9.2]
#                 '/bestvideo[height=1080][vcodec=vp9][fps>30]'
#                 '/bestvideo[height=1080][vcodec=vp9]'
#                 '/bestvideo[vcodec=vp9.2]'
#                 '/bestvideo[vcodec=vp9][fps>30]'
#                 '/bestvideo[vcodec=vp9])'
#                 '/bestvideo')

def is_url(url):
    return urlparse(url).scheme in ('http', 'https',)


def GetFFProbeInfo(path: str):
    result = subprocess.run(
        ['ffprobe', '-v', 'quiet', '-print_format', 'json', '-show_format', '-show_streams',
         str(path)],
        stdout=subprocess.PIPE)
    probe = json.loads(result.stdout)

    return probe['streams'][0]


def GetYTVideoMetadata(url: str):
    ydl_opts = dict(restrictfilenames=True, format=bestformat)
    yt = youtube_dl.YoutubeDL(ydl_opts)
    info = yt.extract_info(url, download=False)
    metadata = dict(vid_id=info['id'],
                    title=info['title'],
                    safetitle=unicodedata.normalize('NFKD', info['title']).encode(
                        'ascii', 'ignore').decode('ascii').strip().translate(str.maketrans('', '', string.punctuation)),
                    desc=info['description'],
                    keywords=info['tags'],
                    url=info['webpage_url'],
                    fps=info['fps'],
                    slugs=['youtube'],
                    width=info['width'],
                    height=info['height'],
                    length=info['duration'])
    return metadata


def GetCmdlVideoMetadata(path):
    questions = [
        inquirer.Text('title', message="Enter the video title", validate=lambda _, x: x is not None and x != ""),
        inquirer.Text('desc', message="Enter the video description (optional)", default=''),
        inquirer.Text('keywords', message="Enter the video keywords (not slugs!) separated by commas (optional)", default=''),
        inquirer.Text('url', message="Enter the video original URL (optional)", default='none'),
        inquirer.Text('slugs', message="Enter the video slugs separated by commas", validate=lambda _, x: x is not None and x != ""),
    ]
    answers = inquirer.prompt(questions)
    probe = GetFFProbeInfo(path)
    fps = Fraction(probe['avg_frame_rate'])
    metadata = dict(vid_id='none',
                    title=answers['title'],
                    safetitle=unicodedata.normalize('NFKD', answers['title']).encode(
                        'ascii', 'ignore').decode('ascii').strip().translate(str.maketrans('', '', string.punctuation)),
                    desc=answers['desc'],
                    keywords=[x.strip() for x in answers['keywords'].split(',')],
                    url=answers['url'],
                    fps=float(round(fps.numerator / Decimal(fps.denominator), 3)),
                    slugs=[x.strip() for x in answers['slugs'].split(',')],
                    width=probe['width'],
                    height=probe['height'],
                    length=int(float(probe['duration'])),
                    codec_name=probe['codec_long_name'],
                    codec=probe['codec_name'])
    if 'nb_frames' in probe: metadata['num_frames'] = int(probe['nb_frames'])
    return metadata


def SaveVidMetadata(meta, vid_path):
    os.makedirs(os.path.dirname(vid_path), exist_ok=True)
    with open(os.path.join(os.path.dirname(vid_path), 'meta.json'), 'w+') as f:
        json.dump(meta, f, indent=4)
    return


def DownloadYTVideo(metadata: dict):
    name = metadata['vid_id'] + '_' + \
        metadata['safetitle'].lower().replace(' ', '_')
    os.makedirs(name, exist_ok=True)
    ydl_opts = dict(restrictfilenames=True, outtmpl=os.path.join(name, name + '.%(ext)s'), format=bestformat)
    yt = youtube_dl.YoutubeDL(ydl_opts)
    yt.download([metadata['url']])
    savedvid_path = glob.glob(os.path.join(name, name) + '*')[0]
    print('Download complete: ' + savedvid_path)
    probe = GetFFProbeInfo(savedvid_path)
    fps = Fraction(probe['avg_frame_rate'])
    metadata['fps'] = float(round(fps.numerator / Decimal(fps.denominator), 3))
    metadata['codec'] = probe['codec_name']
    metadata['codec_name'] = probe['codec_long_name']
    if 'nb_frames' in probe: metadata['num_frames'] = probe['nb_frames']
    return savedvid_path


def DownloadLocalVideo(metadata: dict, vid_path: Path):
    name = metadata['vid_id'] + '_' + \
        metadata['safetitle'].lower().replace(' ', '_')
    os.makedirs(name, exist_ok=True)
    savedvid_path = shutil.copy(vid_path, name)
    return savedvid_path


def DetectScenes(vid_path, thresh):
    scenesfile = os.path.join(os.path.dirname(vid_path), 'scenes.txt')
    extractscenes_cmdl = 'ffmpeg -y -i {} -filter:v \"select=\'gt(scene,{})\',showinfo\" -f null - 2>&1 | tee {}'.format(
        vid_path, thresh, scenesfile)
    spinner = Spinner('Detecting scenes... ')
    process = subprocess.Popen(extractscenes_cmdl, shell=True, stdout=subprocess.PIPE)
    # process.wait()
    while True:
        line = process.stdout.readline()
        spinner.next()
        if not line:
            break
    spinner.finish()
    return scenesfile


def SplitScenes(scenes_path: str, start: str, end: str):
    if (start is None) or (end is None):
        try:
            scenepts_f = open(scenes_path, 'r')
            scenepts_t = scenepts_f.read()
            scenepts_f.close()
        except OSError:
            print('Could not access the scene data file. Exiting.')
            sys.exit(1)
            pass

        scenepts = re.findall(r"\s(pts_time:)\b(\d*\.\d{0,3})\b", scenepts_t)
        duration_re = re.findall(r"(Duration: )(\d{2})\:(\d{2})\:(\d{2})\.(\d{2})", scenepts_t)
        duration = float(duration_re[0][1]) * 3600 + float(duration_re[0][2]) * 60 + float(
            duration_re[0][3]) + float(duration_re[0][4]) / 100
        scenepts = ['0.0'] + [row[1] for row in scenepts] + [str(duration)]
    else:
        startsplit = start.split(':')
        startseconds = float(startsplit[0]) * 3600 + float(startsplit[1]) * 60 + float(startsplit[2])
        endsplit = end.split(':')
        endseconds = float(endsplit[0]) * 3600 + float(endsplit[1]) * 60 + float(endsplit[2])
        scenepts = [startseconds] + [endseconds]

    bar = IncrementalBar('Extracting scenes:', max=len(scenepts) - 1)

    sceneframes = []

    for n, (startpts, endpts) in enumerate(zip(scenepts, scenepts[1:])):
        scenedir = os.path.join(os.path.dirname(savedvid_path),
                                'shot' + f'{n:03}' + '_' + time.strftime('%M_%S', time.gmtime(float(startpts))))
        os.makedirs(scenedir, exist_ok=True)
        extractscene_cmdl = 'ffmpeg -y -ss {} -to {} -i {} -copyts -start_at_zero -frame_pts true -q:v 1 {}'.format(
            startpts, endpts, savedvid_path, os.path.join(scenedir, 'rgb.%07d.jpg'))
        process = subprocess.Popen(extractscene_cmdl, shell=True)
        process.wait()
        num_frames = len([f for f in os.listdir(scenedir) if os.path.isfile(os.path.join(scenedir, f))])
        sceneframes.append((n, num_frames))
        bar.next()
    bar.finish()
    return sceneframes


############################################################################################

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument(
    '-sc',
    '--scenechange',
    type=float,
    default=0.4,
    help='Scenechange detection threshold (0.0..1.0, where lower value means greater sensitivity), default 0.4')
parser.add_argument('url', help='URL to the YouTube video or path to a local video file', type=str)
parser.add_argument('--start', help='Start timestamp', type=str, default=None)
parser.add_argument('--end', help='End timestamp', type=str, default=None)
parser.add_argument('--download_only', action='store_true', help='Do not detect and split into scenes')
args = vars(parser.parse_args())

if is_url(args['url']):
    metadata = GetYTVideoMetadata(args['url'])
    savedvid_path = DownloadYTVideo(metadata)
else:
    vid_path = Path(args['url'])
    if not vid_path.is_file() or not vid_path.exists():
        raise FileNotFoundError('Invalid file path provided')
    metadata = GetCmdlVideoMetadata(vid_path)
    savedvid_path = DownloadLocalVideo(metadata, vid_path)

SaveVidMetadata(metadata, savedvid_path)
if not args['download_only']:
    if (args['start'] is not None) and (args['end'] is None):
        args['end'] = metadata['length']
    elif (args['start'] is None) and (args['end'] is not None):
        args['start'] = 0
    scenes_path = ''
    if (args['start'] is None) and (args['end'] is None):
        scenes_path = DetectScenes(savedvid_path, args['scenechange'])
    scenes_frames = SplitScenes(scenes_path, args['start'], args['end'])
    for (n, c) in scenes_frames:
        print("Scene {}: {} frames".format(n, c))
    print('\nNow review the scenes in the generated directory, and the contents of meta.json')
else:
    print('\nMetadata generation is done, review meta.json')